
from pyspark.sql import DataFrame, SparkSession

from helper import get_spark_session, assert_dataframes_equality


INPUT_FILE_NAME = "data/input.txt"
OUTPUT_FILE_NAME = "data/output.txt"


def read_txt(session: SparkSession, file_path: str) -> DataFrame:
    # TODO: Implement this function to return dataframe by using given file path
    #  right now its just retuning empty dataframe
    return session.createDataFrame([])


def combine_contiguous_segments(df: DataFrame) -> DataFrame:
    # TODO: This function should combine the segments and return final dataframe
    return df


def main():
    session = get_spark_session("nfcu-dim-challenge")

    fixture_df = read_txt(session, INPUT_FILE_NAME)
    result_df = combine_contiguous_segments(fixture_df)

    output_df = read_txt(session, OUTPUT_FILE_NAME)
    assert_dataframes_equality(output_df, result_df)


if __name__ == '__main__':
    main()
