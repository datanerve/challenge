## Challenge Guidelines ##

We are giving you this challenge to have you write code dealing with a
challenge to see you write clear,
maintainable code. This challenge is designed to be open-ended, and you are not
expected to complete all the way (implementation and testing).

We trying to combine date ranges into the maximum date ranges where there is no overlap between multiple records between start and end dates.
The goal is to identify groups of continuous data sequences and groups of data where the sequence is missing.

The data is about NFCU members historical data which contains multiple address records with start and end dates,
based on a variety of member address attributes. Please look at the provided input and the expected output below. 

### Input data: ###
member_id | address_id | address1 | address2 | city | state | zip | start_date | end_date
--- | --- | --- |--- |--- |--- |--- |--- |--- |
1 | 1| 1 bank St| Suite 100| Gaithersburg| MD| 20878| 10-10-1921| 10-10-1928
1 | 1| 1 Bank Street| Suite 100| Gaithersburg| MD| 20878| 10-10-1922| 10-10-1931
1 | 1| 1 Bank St| Suite 100| gaithersburg| MD| 20878| 10-01-1932| 10-01-1932
1 | 1| 1 Bank St| suit 100| Gaithersburg| MARYLAND| 20878| 10-02-1932| 10-10-1942
2 | 2| 13922 Lee Jackson Memorial Hwy| Suite 123| Chantilly| VA| 20151| 12-31-1921| 01-01-1928
2 | 2| 13922 Lee Jackson Memorial Hwy| Suite 123| Chantilly| VA| 20151| 12-31-1927| 10-10-1938
3 | 4| 12851 Worldgate Dr| #500| Herndon| VA| 20170| 10-01-1932| 11-01-1932
3 | 5| 12852 Worldgate Dr| #500| Herndon| VA| 20170| 10-01-1932| 11-01-1932

### Expected Output data: ###
member_id | address_id | address1 | address2 | city | state | zip | start_date | end_date
--- | --- | --- |--- |--- |--- |--- |--- |--- |
1 | 1| 1 Bank Street| Suite 100| Gaithersburg| MD| 20878| 10-10-1921| 10-10-1931
1 | 1| 1 Bank St| Suite 100| Gaithersburg| MARYLAND| 20878| 10-01-1932| 10-01-1942
2 | 2| 13922 Lee Jackson Memorial Hwy| Suite 123| Chantilly| VA| 20151| 12-31-1921| 01-01-1938
3 | 4| 12851 Worldgate Dr| #500| Herndon| VA| 20170| 10-01-1932| 11-01-1932
3 | 5| 12852 Worldgate Dr| #500| Herndon| VA| 20170| 10-01-1932| 11-01-1932


This solution should be implemented using pyspark by creating reusable function and unit tested. Please check data
directory for sample input and output data you will be working.
Do not make any changes to output.txt file as it is just for your reference

We have left couple of functions (read_txt and combine_contiguous_segments) unimplemented in main.py to let you complete the implementation - You can rename, add additional parameters as per your need 


When you are done, you should:
1. Create a `README.md` file with a one-page summary of your approach and what
   you see as next steps.
2. Compress the `challenge` directory (with all the code and `README.md`) and
   attach it to your email.

You should spend about 1 hour for this challenge, excluding the time it takes to write your `README.md`.

Run below command to test the functionality after completion 

    $ python main.py

## Evaluation criteria

Your submission will be evaluated on the following criteria:

* Implementation/Performance
  * Does the code compile/run?
  * Does it pass test?
  * Does it contains documentation in the code
* Written Communication (`README.md`)
  * Is the summary of the approach understandable?
  * Is the summary accurate?
  * Is there awareness of areas of improvement?

Good luck!
