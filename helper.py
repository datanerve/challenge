from pyspark import SparkContext
from pyspark.sql import SparkSession, DataFrame


def get_spark_session(app_name: str) -> SparkSession:
    # This solution only works on local env
    session = (SparkSession
               .builder
               .appName(app_name)
               .master("local[*]")
               .getOrCreate())
    sc: SparkContext = session.sparkContext
    sc.setLogLevel("INFO")
    return session


def assert_dfs_count(fixture_df: DataFrame, actual_df: DataFrame):
    fixture_count = fixture_df.count()
    actual_count = actual_df.count()
    if fixture_count != actual_count:
        fixture_df.show(truncate=False)
        actual_df.show(truncate=False)
    assert fixture_count == actual_count
    return fixture_count, actual_count


def assert_df_not_contains_duplicates(df: DataFrame, df_count=-1) -> None:
    if df_count < 0:
        df_count = df.count()
    df = df.dropDuplicates()
    df_post_count = df.count()
    assert df_count == df_post_count


def assert_dfs_schema_are_equal(fixture_df: DataFrame, actual_df: DataFrame) -> None:
    assert len(fixture_df.dtypes) == len(actual_df.dtypes)
    # symmetric_difference
    diff_schema = set(fixture_df.dtypes) ^ set(actual_df.dtypes)
    if diff_schema:
        fixture_df.printSchema()
        actual_df.printSchema()
    assert not diff_schema


def assert_dfs_contents_are_equal(source_df: DataFrame, target_df: DataFrame) -> None:
    diff_df1 = source_df.subtract(target_df)
    diff_df2 = target_df.subtract(source_df)
    diff_df_count = diff_df1.count() + diff_df2.count()
    if diff_df_count > 0:
        diff_df1.show(truncate=False)
        diff_df2.show(truncate=False)
    assert diff_df_count == 0


def assert_dataframes_equality(fixture_df: DataFrame, actual_df: DataFrame) -> None:
    # 1. Check if both data frames have identical schema - mainly column names and data types
    assert_dfs_schema_are_equal(fixture_df, actual_df)

    # 2. Check row counts
    fixture_count, actual_count = assert_dfs_count(fixture_df, actual_df)

    # 3. Check for duplicates in each data frame
    assert_df_not_contains_duplicates(actual_df, actual_count)

    # 4. Identify data mismatch between two data frames ex: A - B and B - A
    # make sure columns are in same order
    col_ordered_df = actual_df.select(*fixture_df.columns)
    assert_dfs_contents_are_equal(fixture_df, col_ordered_df)
    assert_dfs_contents_are_equal(col_ordered_df, fixture_df)
